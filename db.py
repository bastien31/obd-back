import asyncio
import pprint

SERVER = 'localhost'
PORT = 27017

async def async_get_latest():
    import motor.motor_asyncio

    client = motor.motor_asyncio.AsyncIOMotorClient(SERVER, PORT)

    cursor = client.OBD.obd.find()
    cursor.sort('_id', -1).limit(10)

    data = []
    async for document in cursor:
        data.append(document)

    pprint.pprint(data)
    return data


def sync_get_latest(latest):
    global LATEST
    from pymongo import MongoClient

    client = MongoClient(SERVER, PORT)
    if latest == -1:
        # Retrieve first 1000 records
        cursor = client.OBD.obd.find()
        cursor.sort('_id', -1).limit(1000)
    else:
        # Retrieve new ones
        cursor = client.OBD.obd.find({'_id': {"$gt": latest}})
        cursor.sort('_id', -1).limit(1000)

    data = []
    my_latest = None
    for document in cursor:
        obd_data = {k: document[k] for k in document if k != "_id"}
        obd_data["id"] = str(document["_id"])
        if my_latest is None:
            my_latest = document["_id"]
        data.append(obd_data)

    return data, my_latest or latest

if __name__ == '__main__':
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(async_get_latest())
    sync_get_latest()
