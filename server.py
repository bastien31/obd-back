import asyncio
import json
from threading import Timer

from autobahn.asyncio.websocket import WebSocketServerFactory
from autobahn.asyncio.websocket import WebSocketServerProtocol

from db import sync_get_latest

LOOP = asyncio.get_event_loop()

class MyObdDataServerProtocol(WebSocketServerProtocol):
    _t = None
    _f = 0.5
    _latest_id = -1

    def onConnect(self, request):
        print("Client connecting: {0}".format(request.peer))

    def onOpen(self):
        print("WebSocket connection open.")
        self._set_timer()

    def _set_timer(self):
        self._t = Timer(self._f, self.send_data)
        self._t.start()

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))
        if self._t is not None:
            self._t.cancel()

    def send_data(self):
        latest_data, self._latest_id = sync_get_latest(self._latest_id)
        if latest_data:
            self.sendMessage(str.encode(json.dumps(latest_data)))
        self._set_timer()


if __name__ == '__main__':
    import asyncio

    factory = WebSocketServerFactory(u"ws://127.0.0.1:9001")
    factory.protocol = MyObdDataServerProtocol

    coro = LOOP.create_server(factory, '0.0.0.0', 9001)
    server = LOOP.run_until_complete(coro)

    try:
        LOOP.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        LOOP.close()
